# Exercice 1

On va créer un bucket S3.
1. On va récupérer son login/mdp de la console AWS fourni par le formateur
2. SUr la console, on va générer un accesskey (CLI) et son secret
3. Sur son projet gitlab, on va créer un fichier `main.tf`. Dedans, on va appeler le provider AWS, et rentrer la région, l'access key et le secret key.
4. Dans le `main.tf`, on va ensuite utiliser la ressource `aws_s3_bucket` en récupérer l'exemple de code depuis la documentation provider AWS chez Hashicorp.
5. On fait la séquence :

```bash
$ terraform init
$ terraform plan # <-- On doit voir un add, 0 change, 0 destroy
$ terraform apply
```

# Exercice 2

On a mis les credentials en clair dans le bloc provider. C'est pas terrible, on va ici déplacer les credentials dans des variables.
1. On va créer à la racine du projet, un fichier `variables.tf` où on va déclarer les variables access et secret.
2. On créer un répertoire `.tfvars`, dedans on créé un fichier `secret.tfvars`
3. On modifie le fichier ` main.tf` pour remplacer les credentials par nos variables
4. On refait la séquence des commandes terraform pour voir si ça fonctionne toujours. Logiquement, notre bucket existe déjà sur AWS suite à l'exercice 1. Il n'y aura pas de changement

# Exercice 3

Maintenant, on va stocker le fichier tfstate dans Gitlab, la rubrique "operate" 


# Exercice 4

On va récupérer un code TF du formateur pour le lire. On va l'utiliser.