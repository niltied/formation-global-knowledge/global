#
# MODULE BUCKET + USER
# 
# Module User (qui s'appuie sur le module user)
module "user" {
    source         = "../user"
    name           = var.name
    pgp_key        = var.pgp_key
}

resource "aws_s3_bucket" "monBucket" {
  bucket = "buck-${var.name}"

  tags = {
    Name        = "bucket"
    Environment = "Dev"
  }
}